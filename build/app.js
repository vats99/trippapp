var tripWeb = angular.module('listDemo1', ['ngMaterial']);
tripWeb.config(function($mdIconProvider) {
$mdIconProvider
.iconSet('communication', 'img/icons/sets/communication-icons.svg', 24);
});

tripWeb.service('apiFlightSearch', ['$http', '$timeout', '$q', function($http,$timeout, $q){
function getHeaderParams () {
  return {
      'Content-Type': 'application/json',
      'access-token': '8fc3e40da05dd15092a219514d60bc892fe24cb4b20db5171a9a08d654603fc7'
  };
};

this.getFlight = function (params) {
  var req;
              return $q(function (resolve, reject) {
                var req = $http.post("http://52.76.245.18/tripp-api/web/flightbooking/search", params, {headers: getHeaderParams()});
           req.then(
               function (resp) {
                 if (resp) {
                   resolve(resp);
                 } else {
                     reject(resp);
                 }
               }, reject);
       });

      };
}]);

tripWeb.controller('AppCtrl',['$scope', 'apiFlightSearch', function($scope, apiFlightSearch) {
var imagePath = 'img/profile.png';

$scope.phones = [
  {
    type: 'Home',
    number: '(555) 251-1234',
    options: {
      icon: 'communication:phone'
    }
  },
  {
    type: 'Cell',
    number: '(555) 786-9841',
    options: {
      icon: 'communication:phone',
      avatarIcon: true
    }
  },
  {
    type: 'Office',
    number: '(555) 314-1592',
    options: {
      face : imagePath
    }
  },
  {
    type: 'Offset',
    number: '(555) 192-2010',
    options: {
      offset: true,
      actionIcon: 'communication:phone'
    }
  }
];
$scope.todos = [
  {
    face : imagePath,
    what: 'Brunch this weekend?',
    who: 'Min Li Chan',
    when: '3:08PM',
    notes: " I'll be in your neighborhood doing errands"
  },
  {
    face : imagePath,
    what: 'Brunch this weekend?',
    who: 'Min Li Chan',
    when: '3:08PM',
    notes: " I'll be in your neighborhood doing errands"
  },
  {
    face : imagePath,
    what: 'Brunch this weekend?',
    who: 'Min Li Chan',
    when: '3:08PM',
    notes: " I'll be in your neighborhood doing errands"
  },
  {
    face : imagePath,
    what: 'Brunch this weekend?',
    who: 'Min Li Chan',
    when: '3:08PM',
    notes: " I'll be in your neighborhood doing errands"
  },
  {
    face : imagePath,
    what: 'Brunch this weekend?',
    who: 'Min Li Chan',
    when: '3:08PM',
    notes: " I'll be in your neighborhood doing errands"
  },
];
}]);
tripWeb.controller('DemoCtrl',['$scope','apiFlightSearch', function($scope, apiFlightSearch) {
$scope.view = 'search';
$scope.states = ('BLR MAA DEL').split(' ').map(function(state) {
   return {abbrev: state};
 });
this.userData = {
  isreturn: '',
  onward:'',
  onwardDate:'',
  returnjourney:'',
  returnDate:''
};

 $scope.searchflight = function(session){
     $scope.preloader = true;
     $scope.view = 'searchresult';
     var params = {"adult":1,"isRoundTrip":false,"provider":"tbo","segments":[{"origin":"BLR","destination":"MAA","cabinClass":2,"departureTime":"2016-12-30T00:00:00","arrivalTime":"2016-12-30T00:00:00"}]};
   $scope.flightdata = apiFlightSearch.getFlight(params);
   $scope.flightdata.then(
       function (resp) {
           if (resp) {
             $scope.preloader = false;
               $scope.journeydata = resp.data.Journeys[0];
           }
       },
       function (err) {
       }
   );
   $scope.view = 'searchresult';
 };

}])
.config(function($mdThemingProvider) {
// Configure a dark theme with primary foreground yellow
$mdThemingProvider.theme('docs-dark', 'default')
 .primaryPalette('yellow')
 .dark();

});
